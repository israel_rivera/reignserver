let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let app = express();
var cron = require('node-cron');

let apiRoutes = require("./api-routes");

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json(),
    function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });

mongoose.connect('mongodb://mongo:27017/DataNode', { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose.connection;

if (!db)
    console.log("Error to connect")
else
    console.log("BD connect")

var port = process.env.PORT || 8080;

app.get('/', (req, res) => res.send('HI!!! from root'));
app.use('/api', apiRoutes);


Data = require('./model/model');
cron.schedule('0 * */1 * * *', () => {
    console.log(Date());
    var process = require('./process');
    process.executeTask();
});

app.listen(port, function () {
    console.log("Port listen " + port);
    var process = require('./process'); //execute firstime
    process.executeTask();
});

