// api-routes.js
// Inicializamos express routeo
let router = require('express').Router();
// Seteamos ruta Principal /api/
router.get('/', function (req, res) {
    res.json({
        status: 'Work fine!',
        message: 'Welcome to reign API',
    });
});

var dataController = require('./controller/dataController');

router.route('/data')
    .get(dataController.index)
router.route('/data/:data_id')
    .get(dataController.view)
    .patch(dataController.update)
    .put(dataController.update)
// Exportamos el modulo de ruteo
module.exports = router;

module.exports = router;
