"use strict"

var assert = require('assert');
var request = require('supertest')
var app = require('../index.js')

var request = request("http://localhost:8080")

describe('Data', function () {
    describe('GET', function () {
        it('Should return json as default data format', function (done) {
            request.get('/api/data')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });
        it('Should return status 200 with id', function (done) {
            request.get('/api/data/5e0e11e10b54f78798257675')
                .expect('Content-Type', /json/)
                .expect(200, done)
        });        
    });
    describe('PUT', function () {
        it('should update data Status for logical delete', function (done) {
            let passData = {
                "_id": "5e0e11e10b54f78798257675",
                "Status": "1"
            }
            request.put('/api/data/5e0e11e10b54f78798257675')
                .send(passData)
                .expect('Content-Type', /json/)
                .expect(200, done);                
        });
     });
});