
var mongoose = require('mongoose');
var dataNodeSchema = mongoose.Schema({
    created_at: Date,
    title: String,
    url: String,
    author: String,
    points: String,
    story_text: String,
    comment_text: String,
    num_comments: String,
    story_id: Number,
    story_title: String,
    story_url: String,
    parent_id: Number,
    objectID : Number,
    Status : String
});
// Exportamos modelo
var Data = module.exports = mongoose.model('data', dataNodeSchema);
module.exports.get = function (callback, limit) {
    Data.find(callback).limit(limit);
}