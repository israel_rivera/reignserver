// contactController.js
// Importamos modelo
Data = require('../model/model');
// trigger acciones index
exports.index = function (req, res) {
    Data.find({ Status: "1"},function (err, data) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Get Data all",
            data: data
        });
    });
};

// gatillamos info de contacto
exports.view = function (req, res) {
    Data.findById(req.params.data_id, function (err, data) {
        if (err)
            res.send(err);
        res.json({
            message: 'data load detail by Id',
            data: data
        });
    });
};
// trigger delete logical
exports.update = function (req, res) {
    Data.findById(req.params.data_id, function (err, data) {
        //console.log(req.params);
        if (err)
            res.send(err);
        data.Status = req.body.Status;
        
        data.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'update data ',
                data: data
            });
        });
    });
};

